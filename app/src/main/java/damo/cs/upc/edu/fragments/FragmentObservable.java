package damo.cs.upc.edu.fragments;

import android.app.Fragment;

/**
 * Created by josepm on 27/9/16.
 */
public class FragmentObservable extends Fragment {

    public static final int NOVA_SELECCIO = 100;
    public static final int MODIF_CRIM = 110;
    public static final int EDICIO_CRIM = 120;

    protected void avisaActivity(Crim crim, int idCrida) {
        ((ContracteFragment) getActivity()).onCanviFragment(crim, idCrida);
    }


    public interface ContracteFragment {
        void onCanviFragment(Crim crim, int idCrida);
    }

}
